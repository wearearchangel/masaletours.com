const gulp = require('gulp');
const HubRegistry = require('gulp-hub');
const browserSync = require('browser-sync');
var sassGlob = require('gulp-sass-glob'),
  compass = require('gulp-compass');

const conf = require('./conf/gulp.conf');

// Load some files into the registry
const hub = new HubRegistry([conf.path.tasks('*.js')]);

// Tell gulp to use the tasks just loaded
gulp.registry(hub);

// styles: https://gist.github.com/aaronwaldon/8657432
//gulp.task('styles', function () {
//  return gulp
//    .src('src/assets/scss/*.scss')
//    .pipe(compass({
//      require: [
//        'susy',
//        'breakpoint'
//      ],
//      css:    conf.path.src('assets/css'),
//      sass:   conf.path.src('assets/scss'),
//      images: conf.path.src('assets/images'),
//      fonts:  conf.path.src('assets/fonts'),
//      relativeAssets: true,
//      debug: false
//    }))
//    .pipe(sassGlob())
//    .pipe(gulp.dest('dist/assets/css'));
//});

gulp.task('inject', gulp.series(gulp.parallel('styles', 'scripts'), 'inject'));
gulp.task('build', gulp.series('partials', gulp.parallel('inject', 'other'), 'build'));
gulp.task('test', gulp.series('scripts', 'karma:single-run'));
gulp.task('test:auto', gulp.series('watch', 'karma:auto-run'));
gulp.task('serve', gulp.series('inject', 'watch', 'browsersync'));
gulp.task('serve:dist', gulp.series('default', 'browsersync:dist'));
gulp.task('default', gulp.series('clean', 'build'));
gulp.task('watch', watch);

function reloadBrowserSync(cb) {
  browserSync.reload();
  cb();
}

// watch
function watch(done) {
  gulp.watch([
    conf.path.src('index.html'),
    'bower.json'
  ], gulp.parallel('inject'));

  gulp.watch(conf.path.src('app/**/*.html'), reloadBrowserSync);
  gulp.watch([
    conf.path.src('assets/css/**/*.css')
  ], gulp.series('styles'));
  // gulp.watch('assets/scss/**/*.scss', ['styles']);
  gulp.watch(conf.path.src('**/*.js'), gulp.series('inject'));
  done();
}
