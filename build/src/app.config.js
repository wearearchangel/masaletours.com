'use strict';

/** @ngInject */
//function routesConfig($stateProvider) {
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      views: {
        '': {
          component: 'pages'
        },
        'content@home': {
          templateUrl: 'app/pages/landing.html'
        }
      },
      resolve: {
        activities: function (AppService) {
          return AppService.getAllActivities();
        },
        accommodations: function (AppService) {
          return AppService.getAllAccommodations();
        }
      }
    })
    .state('about', {
      url: '/about',
      views: {
        '': {
          component: 'pages'
        },
        'content@about': {
          templateUrl: 'app/pages/about.html'
        }
      }
    })
    .state('contacts', {
      url: '/contacts',
      views: {
        '': {
          component: 'pages'
        },
        'content@contacts': {
          templateUrl: 'app/pages/contacts.html'
        }
      }
    })
  
    /*--- Data Objects ---*/
  
    .state('activities', {
      url: '/activities',
      views: {
        '': {
          component: 'dock'
        },
        'content@activities': {
          component: 'activities'
        }
      },
      resolve: {
        activities: function (AppService) {
          return AppService.getAllActivities();
        }
      }
    })
    .state('activities.activity', {
      url: '/{activityId}',
      views: {
        '': {
          component: 'cardbox'
        },
        'content@activities.activity': {
          component: 'activity'
        }
      },
      resolve: {
        activity: function (activities, $stateParams) {
          return activities.find(function (activity) {
            return activity.id === $stateParams.activityId;
          });
        }
      }
    })
  
    .state('accommodations', {
      url: '/accommodations',
      views: {
        '': {
          component: 'dock'
        },
        'content@accommodations': {
          component: 'accommodations'
        }
      },
      resolve: {
        accommodations: function (AppService) {
          return AppService.getAllAccommodations();
        }
      }
    })
    .state('accommodations.accommodation', {
      url: '/{accommodationId}',
      views: {
        '': {
          component: 'cardbox'
        },
        'content@accommodations.accommodation': {
          component: 'accommodation'
        }
      },
      resolve: {
        accommodation: function (accommodations, $stateParams) {
          return accommodations.find(function (accommodation) {
            return accommodation.id === $stateParams.accommodationId;
          });
        }
      }
    })
  
    .state('safaris', {
      url: '/safaris',
      views: {
        '': {
          component: 'dock'
        },
        'content@safaris': {
          component: 'safaris'
        }
      },
      resolve: {
        safaris: function (SafarisService) {
          return SafarisService.getAllSafaris();
        }
      }
    })
    .state('safaris.safari', {
      url: '/{safariId}',
      component: 'safari',
      resolve: {
        safari: function (safaris, $stateParams) {
          return safaris.find(function (safari) {
            return safari.id === $stateParams.safariId;
          });
        }
      }
    })
    .state('destinations', {
      url: '/destinations',
      views: {
        '': {
          component: 'dock'
        },
        'content@destinations': {
          component: 'destinations'
        }
      },
      resolve: {
        destinations: function (DestinationsService) {
          return DestinationsService.getAllDestinations();
        }
      }
    })
    .state('destinations.destination', {
      url: '/{destinationId}',
      component: 'destination',
      resolve: {
        destination: function (destinations, $stateParams) {
          return destinations.find(function (destination) {
            return destination.id === $stateParams.destinationId;
          });
        }
      }
    });
}

angular
  .module('app')
  .config(routesConfig);
