angular
  .module('app', [
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'ngMaterial',
    'ngSanitize',
    'ui.router',

    'slickCarousel',
    'scrollToFixed',
    'angularSpinner',
    'angular-ui-view-spinner',
  
//    'landing',
//    'covers',
    'activities',
    'accommodations',
//    'destinations',
//    'safaris'
  ])
  
  // inject us spinner
  .config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({
      lines: 11, // The number of lines to draw
      length: 11, // The length of each line
      width: 5, // The line thickness
      radius: 25, // The radius of the inner circle
      scale: 1, // Scales overall size of the spinner
      corners: 1, // Corner roundness (0..1)
      color: '#5D4052', // #rgb or #rrggbb or array of colors
      opacity: 0, // Opacity of the lines
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      speed: 1, // Rounds per second
      trail: 100, // Afterglow percentage
      fps: 20, // Frames per second when using setTimeout() as a fallback for CSS
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      className: 'spinner', // The CSS class to assign to the spinner
    });
  }])

  // inject the router instance by name
  //  .run(function ($window, ng1UIRouter) {
  //    var vis = $window['ui-router-visualizer'];
  //    vis.visualizer(ng1UIRouter);
  //  })

  /* -------------------------------
      C O N T R O L L E R S
  ------------------------------- */

  // app controller
  .controller('AppController', function ($http, $scope, usSpinnerService) {
    var vm = this;

    // load settings from settings file
    $http.get('app/shared/settings.json').then(function (response) {
      vm.settings = response;
    });
  
    // spinner controller
    $scope.startSpin = function(){
      usSpinnerService.spin('open-cardbox');
    }
    $scope.stopSpin = function(){
      usSpinnerService.stop('open-cardbox');
    }
  
    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      if (toState.resolve) {
          $scope.startSpin();
      }
    });
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      if (toState.resolve) {
          $scope.stopSpin();
      }
    });    
  })

  /* -------------------------------
      D I R E C T I V E S
  ------------------------------- */

  // back button
  .directive('backButton', function ($window) {
    return {
      restrict: 'A',
      link: function (scope, elem) {
        elem.bind('click', function () {
          $window.history.back();
        });
      }
    };
  })

  // close this
  .directive('closeThis', function ($state) {
    return {
      restrict: 'A',
      link: function (scope, elem) {
        elem.bind('click', function () {
          $state.go('^');
        });
      }
    };
  })

  // go home
  .directive('goHome', function ($state) {
    return {
      restrict: 'A',
      link: function (scope, elem) {
        elem.bind('click', function () {
          $state.go('home');
        });
      }
    };
  })

  // side nav
  .directive('openSideNav', function ($mdSidenav) {
    return {
      restrict: 'A',
      link: function (scope, elem) {
        elem.bind('click', function () {
          $mdSidenav('sidenav').toggle();
        });
      }
    };
  })
;
