'use strict';

angular
  .module('app')
  .service('AppService', function ($http) {
    var service = {
      
      /* -------------------------------
          A C T I V I T I E S
      ------------------------------- */
      
      getAllActivities: function () {
        return $http.get('app/activities/activities.json', {
          cache: false
        }).then(function (resp) {
          return resp.data;
        });
      },

      getActivity: function (id) {
        function activityMatchesParam(activity) {
          return activity.id === id;
        }

        return service.getAllActivities().then(function (activities) {
          return activities.find(activityMatchesParam);
        });
      },
      
      /* -------------------------------
          A C C O M M O D A T I O N S
      ------------------------------- */
      
      getAllAccommodations: function () {
        return $http.get('app/accommodations/accommodations.json', {
          cache: false
        }).then(function (resp) {
          return resp.data;
        });
      },

      getAccommodation: function (id) {
        function accommodationMatchesParam(accommodation) {
          return accommodation.id === id;
        }

        return service.getAllAccommodations().then(function (accommodations) {
          return accommodations.find(accommodationMatchesParam);
        });
      }
    };

    return service;
  });