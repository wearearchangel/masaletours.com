// Define 'activities' module to be used by all dependencies of this feature
angular.module('tripPlanner', [])
.controller('TripPlannerController', ['$scope', function($scope) {
}])

// Container
.directive('tpContainer', function(){  
  return {
    restrict: 'EA',
//    transclude: true,
    controller: ['$scope', function TripPlannerController($scope) {
      var max /* No limit */,
          min = 0 /* Limit to 0 */;

//      $scope.tripPlanner = {
//        count: min /* Start counter at min number */,
//        countMin: min,
//        countMax: max
//      }
//      $scope.tripPlanner = {
//        count: min /* Start counter at min number */,
//        countMin: min,
//        countMax: max
//      }
//      $scope.listing = {
//        tooltipIcon: '',
//        tooltipText: ''
//      }
    }]
//    templateUrl: 'app/trip-planner/trip-planner-container.tpl.html'
  }
})

// Listing Counter
.directive('tpCounter', function(){
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: 'app/trip-planner/trip-planner-counter.tpl.html',
    link: function (scope, elem, attrs) {
      var max /* No limit */,
          min = 0 /* Limit to 0 */;

      scope.tripPlanner = {
        count: min /* Start counter at min number */,
        countMin: min,
        countMax: max
      }
    }
  }
})
// Listing
.directive('tpListing', function(){
  return {
    restrict: 'A',
    templateUrl: 'app/trip-planner/trip-planner-listing.tpl.html'
  }
})

// Trigger
.directive('tpTrigger', function(){
  return {
    restrict: 'A',
    transclude: true,
    scope: true,
    templateUrl: function(elem, attr) {
      if (attr.cta == null) {
        attr.cta = 'default';
      }
      return 'app/trip-planner/trip-planner-trigger-' + attr.cta + '.tpl.html';
    },
    link: function (scope, elem, attrs) {  
      var tpTriggerEl = elem,
          tpTriggerButton = tpTriggerEl.children('.icon__planner'),
          max = 1 /* No limit */,
          min = 0 /* Limit to 0 */;

      scope.listing = {
        count: min /* Start counter at min number */,
        countMin: min,
        countMax: max,
        tooltipIcon: '',
        tooltipText: ''
      }

      // asign default values
      tpTriggerButton.addClass('md-icon-button icon__planner');
      tpTriggerButton.attr('aria-label', scope.listing.tooltipText);

      // load conditional classes
      switch (tpTriggerButton.hasClass('add-to-listing')) {
        case 'add-to-listing':
          scope.listing.tooltipIcon = 'favorite_border';
          scope.listing.tooltipText = 'Add to trip planner';
          break;
        case 'added-to-listing':
          scope.listing.tooltipIcon = 'favorite';
          scope.listing.tooltipText = 'Added to trip planner';
          break;
        default:
          tpTriggerButton.addClass('add-to-listing');
          scope.listing.tooltipIcon = 'favorite_border';
          scope.listing.tooltipText = 'Add to trip planner';
      }

      // handle click events
      elem.bind('click', function ($scope, elem, attrs) {
        // if not already added to listing, add to listing
        if(tpTriggerButton.hasClass('add-to-listing')) {
          tpTriggerButton
            .removeClass('add-to-listing').addClass('added-to-listing')
            .attr('aria-label', 'Added to trip planner');
          scope.listing.tooltipIcon = 'favorite';
          scope.listing.tooltipText = 'Added to trip planner';
          // Increment counter
          if (scope.listing.count >= scope.listing.countMax) {
            return;
          }
          scope.listing.count++;
          scope.tripPlanner.count++;
          console.log(scope.listing.count, scope.tripPlanner.count);
        }
        // if already added to listing, remove from listing
        else if(tpTriggerButton.hasClass('added-to-listing')) {
          tpTriggerButton
            .removeClass('added-to-listing').addClass('add-to-listing')
            .attr('aria-label', 'Add to trip planner');
          scope.listing.tooltipText = 'Add to trip planner';
          scope.listing.tooltipIcon = 'favorite_border';
          // Decrement counter
          if (scope.listing.count <= scope.listing.countMin) {
            return;
          }
          scope.listing.count--;
          scope.tripPlanner.count--;
          console.log(scope.listing.count, scope.tripPlanner.count);
        }
      });
      console.log(scope.listing.count);
      console.log(scope.tripPlanner.count);
    }
  }
})
;
