'use strict';
/* Register `trip planner` component, along with its associated controller and template */
function TripPlannerController($scope) {
  var tripPlannerBtn /* directive */;
  var max /* No limit */;
  var min = 0 /* Limit to 0 */;
  $scope.count = min /* Start counter at min number */;

  $scope.increment = function() {
    if ($scope.count >= max) { return; }
    $scope.count++;
  };
  $scope.decrement = function() {
    if ($scope.count <= min) { return; }
    $scope.count--;
  };
  console.log($scope.count);
}

angular
  .module('tripPlanner')

  // trip planner container directive
  .directive('tpContainer', function () {
    return {
      restrict: 'A',
      scope: {
//        count: '=?'
      },
//      controller: ['$scope', TripPlannerController],
      link: function (scope, elem, attrs) {
        var tripPlannerBtn /* directive */;
        var max /* No limit */;
        var min = 0 /* Limit to 0 */;
        scope.count = min /* Start counter at min number */;
      }      
    }
  })

  .directive('tripPlannerCounter', function () {
    return {
      require: '^^tripPlanner',
      restrict: 'A',
      transclude: true,
      scope: {
        count: '=?'
      },
      controller: function ($scope) {},
      link: function (scope, elem, attrs) {}
    }
  })

  .directive('tripPlannerListing', function () {
    return {
      require: '^^tripPlanner',
      restrict: 'A',
      transclude: true,
      scope: {
        count: '=?'
      },
      controller: function ($scope) {},
      link: function (scope, elem, attrs) {}
    }
  })

  .directive('tripPlannerButton', function () {
    return {
      require: '^^tripPlanner',
      restrict: 'A',
      transclude: true,
      scope: {
        count: '=?'
      },
      controller: function ($scope) {},
//      controller: ['$scope', AddToTripPlannerController],
      link: function (scope, elem, attrs) {    
        var max /* No limit */;
        var min = 0 /* Limit to 0 */;
        scope.count = min /* Start counter at min number */;
        
        // on remove from trip planner
        if(elem.hasClass('add-to-tp')) {
          elem.children('md-icon').html('favorite_border');
          scope.tooltipText = 'Add to trip planner';
        }
        // on add to trip planner
        else if(elem.hasClass('remove-from-tp')) {
          elem.children('md-icon').html('favorite');
          scope.tooltipText = 'Remove from trip planner';
        }
        // instanciate to add to trip planner by default
        else {
          elem
            .addClass('add-to-tp')
            .children('md-icon').html('favorite_border');
          scope.tooltipText = 'Add to trip planner';
        }
        // handle click events
        elem.bind('click', function () {
          // add to trip planner
          if(elem.hasClass('add-to-tp')) {
            elem
              .removeClass('add-to-tp')
              .addClass('remove-from-tp')
              .children('md-icon').html('favorite');
            // Display instructions
            scope.tooltipText = 'Remove from trip planner';
            // Increment counter
            if (scope.count >= max) { return; }
            scope.count++;
          }
          // remove from trip planner
          else if(elem.hasClass('remove-from-tp')) {
            elem
              .removeClass('remove-from-tp')
              .addClass('add-to-tp')
              .children('md-icon').html('favorite_border');
            // Display instructions
            scope.tooltipText = 'Add to trip planner';
            // Decrement counter
            if (scope.count <= min) { return; }
            scope.count--;
          }
        });
      }
    };
  })
;