'use strict';

angular
  .module('safaris')
  .service('SafarisService', function ($http) {
    var service = {
      getAllSafaris: function () {
        return $http.get('app/safaris/safaris.json', {
          cache: false
        }).then(function (resp) {
          return resp.data;
        });
      },

      getSafari: function (id) {
        function safariMatchesParam(safari) {
          return safari.id === id;
        }

        return service.getAllSafaris().then(function (safaris) {
          return safaris.find(safariMatchesParam);
        });
      }
    };

    return service;
  });
