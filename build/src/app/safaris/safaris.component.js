'use strict';
/* Register `safaris` component, along with its associated controller and template*/

angular
  .module('safaris')
  .component('safaris', {
    bindings: {safaris: '<'},
    templateUrl: 'app/safaris/safaris.tpl.html'
  });
