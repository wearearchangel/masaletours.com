'use strict';

// Register `safariList` component, along with its associated controller and template
function SafariBlockController($http) {
  var vm = this;

  $http.get('app/safaris/safaris.json').then(function (response) {
    vm.safaris = response.data;
  });

  // Configure Slick
  vm.slickConfig = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 3,
    autoPlay: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
}

angular
  .module('safaris')
  .component('safariBlock', {
    templateUrl: 'app/safaris/safari-block.tpl.html',
    controller: ['$http', SafariBlockController]
  });
