'use strict';
/* Register `safari` component, along with its associated controller and template*/

angular
  .module('safaris')
  .component('safari', {
    bindings: {safari: '<'},
    templateUrl: 'app/safaris/safari.tpl.html'
  });
