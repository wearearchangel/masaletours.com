'use strict';
/* Register `destination` component, along with its associated controller and template*/

angular
  .module('destinations')
  .component('destination', {
    bindings: {destination: '<'},
    templateUrl: 'app/destinations/destination.tpl.html'
  });
