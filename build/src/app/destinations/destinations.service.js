'use strict';

angular
  .module('destinations')
  .service('DestinationsService', function ($http) {
    var service = {
      getAllDestinations: function () {
        return $http.get('app/destinations/destinations.json', {
          cache: false
        }).then(function (resp) {
          return resp.data;
        });
      },

      getDestination: function (id) {
        function destinationMatchesParam(destination) {
          return destination.id === id;
        }

        return service.getAllDestinations().then(function (destinations) {
          return destinations.find(destinationMatchesParam);
        });
      }
    };

    return service;
  });
