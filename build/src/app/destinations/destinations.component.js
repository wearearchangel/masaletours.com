'use strict';
/* Register `destinations` component, along with its associated controller and template*/

angular
  .module('destinations')
  .component('destinations', {
    bindings: {destinations: '<'},
    templateUrl: 'app/destinations/destinations.tpl.html'
  });
