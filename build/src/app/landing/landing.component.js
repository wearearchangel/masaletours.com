'use strict';
/* Register `landingPage` component, along with its associated controller and template*/
function LandingController($http) {
  var self = this;

  $http.get('app/covers/covers.json').then(function (response) {
    self.covers = response.data;
  });

  $http.get('app/activities/activities.json').then(function (response) {
    self.activities = response.data;
  });

  $http.get('app/destinations/destinations.json').then(function (response) {
    self.destinations = response.data;
  });

  $http.get('app/accommodations/accommodations.json').then(function (response) {
    self.accommodations = response.data;
  });

  $http.get('app/safaris/safaris.json').then(function (response) {
    self.safaris = response.data;
  });

  // Configure Slick
  self.slickConfig = {
    autoPlay: false,
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  };
}

angular
  .module('landing')
  .component('landing', {
    templateUrl: 'app/landing/landing.tpl.html',
    controller: ['$http', LandingController]
  });

