'use strict';

function AccommodationBlockController($http) {
  var vm = this;

  $http.get('app/accommodations/accommodations.json').then(function (response) {
    vm.accommodations = response.data;
  });

  // Configure Slick
  vm.slickConfig = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 3,
    autoPlay: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
}

angular
  .module('accommodations')
  .component('accommodationBlock', {
    templateUrl: 'app/accommodations/accommodation-block.tpl.html',
    controller: ['$http', AccommodationBlockController]
  });
