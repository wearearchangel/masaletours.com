'use strict';

function AccommodationController($http) {
  var self = this;
}

angular
  .module('accommodations')
  .component('accommodation', {
    bindings: {
      accommodation: '<'
    },
    templateUrl: 'app/accommodations/accommodation.tpl.html',
    controller: ['$http', AccommodationController]
  });
