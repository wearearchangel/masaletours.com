'use strict';

function AccommodationsController($http) {
  var self = this;
}

angular
  .module('accommodations')
  .component('accommodations', {
    bindings: {
      accommodations: '<',
    },
    templateUrl: 'app/accommodations/accommodations.tpl.html',
    controller: ['$http', AccommodationsController]
  });
