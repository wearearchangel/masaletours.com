'use strict';

// Register `destinationList` component, along with its associated controller and template
function CoverImageController() {
}

angular
  .module('covers')
  .component('coverImage', {
    templateUrl: 'covers/cover-image.tpl.html',
    controller: CoverImageController
  });
