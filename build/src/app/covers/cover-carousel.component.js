'use strict';

// Register `destinationList` component, along with its associated controller and template
function CoverCarouselController($http) {
  var vm = this;

  $http.get('app/covers/covers.json').then(function (response) {
    vm.covers = response.data;
  });

  // Configure Slick
  vm.slickConfig = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoPlay: true
  };
}

angular
  .module('covers')
  .component('coverCarousel', {
    templateUrl: 'app/covers/cover-carousel.tpl.html',
    controller: CoverCarouselController
  });
