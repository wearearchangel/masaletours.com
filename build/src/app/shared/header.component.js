'use strict';
/* Register `trip planner` component, along with its associated controller and template */
function HeaderController($scope) {
}

angular
  .module('app')
  .component('headerBlock', {
    templateUrl: 'app/shared/header.tpl.html',
    controller: ['$scope', HeaderController]
  })
;