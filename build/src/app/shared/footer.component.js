'use strict';
/* Register `trip planner` component, along with its associated controller and template */
function FooterController($scope) {
}

angular
  .module('app')
  .component('footerBlock', {
    templateUrl: 'app/shared/footer.tpl.html',
    controller: ['$scope', FooterController]
  })
;