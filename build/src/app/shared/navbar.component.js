'use strict';
/* Register `navbar` component, along with its associated controller and template */
function NavbarController($http) {
  var self = this;

  $http.get('app/shared/navbar.json').then(function (response) {
    self.navitems = response.data;
  });
}

angular
  .module('app')
  .component('navbar', {
    templateUrl: 'app/shared/navbar.tpl.html',
    controller: ['$http', NavbarController]
  });
