'use strict';
/* Register `navbar` component, along with its associated controller and template*/
function LogoController($http) {
  var self = this;

  $http.get('app/shared/settings.json').then(function (response) {
    self.settings = response.data;
  });
}

angular
  .module('app')
  .component('logo', {
    templateUrl: 'app/shared/logo.tpl.html',
    controller: ['$http', LogoController]
  });
