'use strict';
/* Register `navbar-docked` component, along with its associated controller and template*/
function NavbarDockedController() {
}

angular
  .module('app')
  .component('navbarDocked', {
    templateUrl: 'app/shared/navbar-docked.tpl.html',
    controller: ['$http', NavbarDockedController]
  });
