'use strict';

function ActivitiesController($http) {
  var self = this;
}

angular
  .module('activities')
  .component('activities', {
    bindings: {
      activities: '<'
    },
    templateUrl: 'app/activities/activities.tpl.html',
    controller: ['$http', ActivitiesController]
  });