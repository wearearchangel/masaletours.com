'use strict';

function ActivityBlockController($http) {
  var vm = this;

  $http.get('app/activities/activities.json').then(function (response) {
    vm.activities = response.data;
  });

  // Configure Slick
  vm.slickConfig = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 3,
    autoPlay: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
}

angular
  .module('activities')
  .component('activityBlock', {
    templateUrl: 'app/activities/activity-block.tpl.html',
    controller: ['$http', ActivityBlockController]
  });
