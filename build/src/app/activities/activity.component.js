'use strict';

function ActivityController($http) {
  var self = this;
}

angular
  .module('activities')
  .component('activity', {
    bindings: {
      activity: '<'
    },
    templateUrl: 'app/activities/activity.tpl.html',
    controller: ['$http', ActivityController]
  });